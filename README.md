# OpenML dataset: schizo

https://www.openml.org/d/45687

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Schizophrenic Eye-Tracking Data in Rubin and Wu (1997)
Biometrics. Yingnian Wu (wu@hustat.harvard.edu) [14/Oct/97]


Information about the dataset
CLASSTYPE: nominal
CLASSINDEX: last

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45687) of an [OpenML dataset](https://www.openml.org/d/45687). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45687/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45687/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45687/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

